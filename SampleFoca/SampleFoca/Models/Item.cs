﻿using System;
using System.Collections.Generic;

namespace SampleFoca.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string canteenSite { get; set; }
        public string dailyMeal { get; set; }      
        public DateTime date { get; set; }
        public Boolean available { get; set; }

        public List<MealCourse> mealCoursesList { get; set; }

        public string mealCoursesText { get; set; }



    }
}