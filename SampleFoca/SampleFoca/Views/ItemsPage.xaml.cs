﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SampleFoca.Models;
using SampleFoca.Views;
using SampleFoca.ViewModels;
using SampleFoca.Services;

namespace SampleFoca.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemsPage : ContentPage
	{
        ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Item;
            if (item == null)
                return;

            await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));

            // Manually deselect item.
            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }


        void OnRefresh(object sender, EventArgs e)
        {
         
            this.IsBusy = true;
            viewModel.Items.Clear();
            
             var entries = MockApiClient.GetMockFoodOptions();
             foreach (Item dayOption in entries)
             {
                dayOption.Id = Guid.NewGuid().ToString();

                StringBuilder builder = new StringBuilder();
                foreach(MealCourse meal in dayOption.mealCoursesList)
                {
                    builder.Append(meal.courseOrder);
                    builder.Append("- ");
                    builder.AppendLine(meal.foodOptionDescription);
                }
                dayOption.mealCoursesText = (dayOption.mealCoursesList.Count > 0) ? builder.ToString() : "";

                viewModel.Items.Add(dayOption);
             }
            
             this.IsBusy = false;
            
        }
    }
}