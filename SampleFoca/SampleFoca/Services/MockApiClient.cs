﻿using Newtonsoft.Json;
using SampleFoca.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleFoca.Services
{
    public class MockApiClient
    {
        public static List<Item> GetMockFoodOptions()
        {
            return FromJsonData( mock_json);
        }

        public static List<Item> FromJsonData(string jsonData)
        {
            List<Item> menusToReturn = JsonConvert.DeserializeObject<List<Item>>(jsonData);
            return menusToReturn;
        }


        public const string mock_json = @"[
    {
        ""canteenSite"": ""Refeitório de Santiago"",
        ""dailyMeal"": ""Almoço"",
        ""mealCoursesList"": [
            {
                ""courseOrder"": 0,
                ""foodOptionDescription"": ""Sopa de legumes""
            },
            {
                ""courseOrder"": 1,
                ""foodOptionDescription"": ""Bife de porco de cebolada com batata cozida""
            }
        ],
        ""date"": ""2017-12-27T00:00:00+00:00"",
        ""available"": true
    },
    {
        ""canteenSite"": ""Refeitório de Santiago"",
        ""dailyMeal"": ""Jantar"",
        ""mealCoursesList"": [
            {
                ""courseOrder"": 0,
                ""foodOptionDescription"": ""Creme de abóbora""
            },
            {
                ""courseOrder"": 1,
                ""foodOptionDescription"": ""Jardineira de salsicha""
            }
        ],
        ""date"": ""2017-12-26T00:00:00+00:00"",
        ""available"": true
    }
]";

    }
}
